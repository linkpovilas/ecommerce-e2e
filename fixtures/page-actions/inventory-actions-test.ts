import { test as base } from "../page-objects/page-objects-test";

type ItemNameOrIndex = string | number;

interface InventoryAction {
  itemNameOf: (index: number) => Promise<string>;
  addToCart: (item: ItemNameOrIndex) => Promise<void>;
  removeFromCart: (item: ItemNameOrIndex) => Promise<void>;
}

export const test = base.extend<InventoryAction>({
  itemNameOf: async ({ inventoryItem }, use) => {
    await use(async (index: number) => {
      const itemName = await inventoryItem.name.nth(index).textContent();
      if (!itemName) {
        throw new Error(`Could not get item name for index: [${index}]`);
      }
      return itemName;
    });
  },
  addToCart: async ({ inventoryItem }, use) => {
    await use(async (item: ItemNameOrIndex) => {
      if (typeof item === "number") {
        return inventoryItem.addToCartButton.nth(item).click();
      }
      await inventoryItem.widget
        .filter({ hasText: item })
        .locator(inventoryItem.addToCartButton)
        .click();
    });
  },
  removeFromCart: async ({ inventoryItem }, use) => {
    await use(async (item: ItemNameOrIndex) => {
      if (typeof item === "number") {
        return inventoryItem.removeFromCartButton.nth(item).click();
      }
      await inventoryItem.widget
        .filter({ hasText: item })
        .locator(inventoryItem.removeFromCartButton)
        .click();
    });
  },
});
