import { test as base } from "../page-objects/page-objects-test";

export interface User {
  username: string;
  password: string;
}

interface LoginAction {
  navigateToLandingPage: () => Promise<void>;
  enterUsername: (username: string) => Promise<void>;
  enterPassword: (password: string) => Promise<void>;
  clickLoginButton: () => Promise<void>;
  loginAs: (user: User) => Promise<void>;
}

export const test = base.extend<LoginAction>({
  navigateToLandingPage: async ({ page }, use) => {
    await use(async () => {
      await page.goto("/");
    });
  },
  enterUsername: async ({ loginBox }, use) => {
    await use(async (username: string) => {
      await loginBox.usernameField.fill(username);
    });
  },
  enterPassword: async ({ loginBox }, use) => {
    await use(async (password: string) => {
      await loginBox.passwordField.fill(password);
    });
  },
  clickLoginButton: async ({ loginBox }, use) => {
    await use(async () => {
      await loginBox.loginButton.click();
    });
  },
  loginAs: async (
    { navigateToLandingPage, enterUsername, enterPassword, clickLoginButton },
    use
  ) => {
    await use(async (user: User) => {
      await navigateToLandingPage();
      await enterUsername(user.username);
      await enterPassword(user.password);
      await clickLoginButton();
    });
  },
});
