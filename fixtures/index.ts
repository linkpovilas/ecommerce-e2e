import { mergeTests } from "@playwright/test";

import { test as loginTest } from "./page-actions/login-actions-test";
import { test as inventoryTest } from "./page-actions/inventory-actions-test";

const test = mergeTests(loginTest, inventoryTest);

export { test, test as it };
export { expect } from "@playwright/test";
