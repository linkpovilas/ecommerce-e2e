import { test as base } from "@playwright/test";
import { LoginBox } from "../../page-objects/authentication/login-box";
import { ErrorMessageContainer } from "../../page-objects/errors/error-message-container";
import { InventoryItem } from "../../page-objects/inventory/inventory-item";
import { CartItem } from "../../page-objects/inventory/cart-item";

interface PageObject {
  loginBox: LoginBox;
  errorMessageContainer: ErrorMessageContainer;
  inventoryItem: InventoryItem;
  cartItem: CartItem;
}

export const test = base.extend<PageObject>({
  loginBox: async ({ page }, use) => {
    await use(new LoginBox(page));
  },
  errorMessageContainer: async ({ page }, use) => {
    await use(new ErrorMessageContainer(page));
  },
  inventoryItem: async ({ page }, use) => {
    await use(new InventoryItem(page));
  },
  cartItem: async ({ page }, use) => {
    await use(new CartItem(page));
  },
});
