import { it, expect } from "../fixtures";
import { errorMessage, testUser, pageUrl } from "../data";

it.describe("Authentication", () => {
  it.beforeEach(async ({ navigateToLandingPage }) => {
    await navigateToLandingPage();
  });

  it("should require username", async ({
    clickLoginButton,
    errorMessageContainer,
  }) => {
    await clickLoginButton();
    await expect(errorMessageContainer.message).toHaveText(
      errorMessage.usernameIsRequired
    );
  });

  it("should require password", async ({
    enterUsername,
    clickLoginButton,
    errorMessageContainer,
  }) => {
    await enterUsername(testUser.standard.username);
    await clickLoginButton();
    await expect(errorMessageContainer.message).toHaveText(
      errorMessage.passwordIsRequired
    );
  });

  it("should allow standard user to login", async ({ loginAs, page }) => {
    await loginAs(testUser.standard);
    await expect(page).toHaveURL(pageUrl.inventory);
  });

  it("should display error message for locked out user", async ({
    loginAs,
    errorMessageContainer,
  }) => {
    await loginAs(testUser.lockedOutUser);
    await expect(errorMessageContainer.message).toHaveText(
      errorMessage.userLockedOut
    );
  });
});
