import { it, expect } from "../fixtures";
import { pageUrl, testUser } from "../data";

it.describe("Shopping cart", () => {
  const FIRST_ITEM = 0;

  it.beforeEach(async ({ loginAs, page, cartItem }) => {
    await loginAs(testUser.standard);
    await expect(page).toHaveURL(pageUrl.inventory);
    await expect(cartItem.shoppingCartBadge).not.toBeVisible();
  });

  it("should allow to add an item", async ({ addToCart, cartItem }) => {
    await addToCart(FIRST_ITEM);
    await expect(cartItem.shoppingCartBadge).toHaveText("1");
  });

  it("should allow to remove items", async ({
    itemNameOf,
    addToCart,
    cartItem,
    removeFromCart,
  }) => {
    const SECOND_ITEM = 1;
    const firstItemName = await itemNameOf(FIRST_ITEM);
    const secondItemName = await itemNameOf(SECOND_ITEM);

    await addToCart(firstItemName);
    await addToCart(secondItemName);
    await expect(cartItem.shoppingCartBadge).toHaveText("2");

    await removeFromCart(firstItemName);
    await expect(cartItem.shoppingCartBadge).toHaveText("1");

    await removeFromCart(secondItemName);
    await expect(cartItem.shoppingCartBadge).not.toBeVisible();
  });
});
