export const errorMessage = {
  userLockedOut: /Sorry, this user has been locked out/i,
  usernameIsRequired: /Username is required/i,
  passwordIsRequired: /Password is required/i,
} as const;
