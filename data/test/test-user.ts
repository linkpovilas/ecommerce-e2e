import { User } from "../../fixtures/page-actions/login-actions-test";

interface TestUser {
  [key: string]: User;
}

export const testUser: TestUser = {
  standard: {
    username: "standard_user",
    password: "secret_sauce",
  },
  lockedOutUser: {
    username: "locked_out_user",
    password: "secret_sauce",
  },
} as const;
