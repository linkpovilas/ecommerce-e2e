import { PageObject } from "../page-object";

export class CartItem extends PageObject {
  get shoppingCartBadge() {
    return this.page.getByTestId("shopping-cart-badge");
  }
}
