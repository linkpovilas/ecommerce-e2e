import { PageObject } from "../page-object";

export class InventoryItem extends PageObject {
  get widget() {
    return this.page.getByTestId("inventory-item");
  }

  get name() {
    return this.page.getByTestId("inventory-item-name");
  }

  get addToCartButton() {
    return this.page.getByTestId(/add-to-cart/);
  }

  get removeFromCartButton() {
    return this.page.getByTestId(/remove/);
  }
}
