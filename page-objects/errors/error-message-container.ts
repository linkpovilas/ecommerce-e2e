import { PageObject } from "../page-object";

export class ErrorMessageContainer extends PageObject {
  get message() {
    return this.page.getByTestId("error");
  }
}
